package com.nova.solutions.junit.mockito.example.mockito.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import java.util.ArrayList;
import java.util.List;

@TestInstance(Lifecycle.PER_CLASS)
public class MockitoUtil {  
  
  List<String> listMock;
  List<String> listSpy;
  
  @BeforeAll
  void setUp() {
    MockitoAnnotations.openMocks(this);

    listMock = Mockito.mock(List.class);
    listSpy = Mockito.spy(ArrayList.class);    
  }
  
  @Test
  void testMockList() {    
    Mockito.when(listMock.add("Nova")).thenThrow(new RuntimeException());
    Assertions.assertThrows(RuntimeException.class, () -> listMock.add("Nova"));
  }
  
  @Test
  void testSpyList() {    
    Mockito.when(listSpy.size()).thenReturn(100);
    
    listSpy.add("one");
    listSpy.add("two");
    
    System.out.println(listSpy.size());
    System.out.println(listSpy.get(0));
  }
}
